const data = require("../data/wookiee_data.json");

module.exports = {
    name: "wookiee",
    description: "WookieeBot talker",
    talk: function(message, args) {
        let talk = data[Math.floor(Math.random()*data.length)];
        message.channel.send(talk);
    },
    execute: function(client, input, args) {

    }
}