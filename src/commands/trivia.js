var data = require("../data/trivia_data.json");
const { MessageEmbed } = require("discord.js");

module.exports = {
    name: "fact",
    description: "Generate facts about the Star Wars universe",

    execute: function(client, message, args) {
        let fact = data.facts[Math.floor(Math.random()*data.facts.length)];
        let embed = new MessageEmbed().setTitle(fact.intro).setColor(0xD2691E).setDescription(fact.fact);
        message.channel.send(embed);
    }
}