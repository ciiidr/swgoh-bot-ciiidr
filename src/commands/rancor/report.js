const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const rancorUtils = require("./utils.js");
const database = require("../../database");

module.exports = {
    name: "rancor.report",
    aliases: ["shitpit.report"],
    description: "Generate a CSV report from the current Rancor/ShitPit.",
    execute: async function(client, input, args, sfw) {
        if (!args || args.length == 0) {
            input.channel.send("Please provide the channel from which to uninstall the " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor.");
            return;
        }

        let channelId = discordUtils.cleanDiscordId(args[0]);
        
        var monitorChannel = client.channels.cache.get(channelId);
        
        if (!monitorChannel) {
            input.channel.send("You cannot do that to a channel that doesn't exist or is in another server.");
            return;
        }

        // check if the monitor is already installed in the channel
        var rancorMonitor = await rancorUtils.getRancorMonitor(channelId);
        
        if (!rancorMonitor) {
            input.channel.send("You do not have a " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor set up in " + args[0] + ". Install " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor with " + rancorUtils.getRancorOrShitPitWord(sfw).toLowerCase() + ".install.");
            return;
        }

        let rmd = await database.db.any(
            `SELECT player, phase, sum(damage_percent) as damage_percent, player_name FROM rancor_monitor_data 
            WHERE rancor_monitor_id = $1 AND posted = TRUE
            group by (player, phase, posted, player_name)
            order by phase ASC`, 
            rancorMonitor.rancor_monitor_id);
        
        var phaseSummariesCSV = "Phase,Name,Damage%";
        for (var rmdi = 0; rmdi < rmd.length; rmdi++)
        {
            var record = rmd[rmdi];
            let name;
            if (record.player_name && record.player_name.length > 0) name = record.player_name;
            else {
                let id = record.player;
                if (discordUtils.isDiscordTag(id)) id = discordUtils.cleanDiscordId(id);
                try {
                    name = (await client.users.fetch(id)).username;
                } catch (e)
                {
                    name = id;
                }
            }
            phaseSummariesCSV += "\r\n" + record.phase + "," + name + "," + record.damage_percent
        }

        let buffer = Buffer.from(phaseSummariesCSV);

        input.channel.send({
            files: [{
              attachment: buffer,
              name: rancorUtils.getRancorOrShitPitWord(sfw) + "_report_" + Date.now() + ".csv"
            }]
          });
    }
}
