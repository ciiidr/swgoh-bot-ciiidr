const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const rancorUtils = require("./utils.js");
const database = require("../../database");

module.exports = {
    name: "rancor.threshold",
    aliases: ["shitpit.threshold", "rancor.t", "shitpit.t"],
    description: "Adjust the threshold for a WookieeBot Rancor/ShitPit monitor.",
    execute: async function(client, input, args, sfw) {
        if (!args || args.length == 0) {
            input.channel.send("You need to provide a channel and a % threshold for when the monitor should turn to a " + discordUtils.symbols.ok + " for total damage.");
            return;
        }
        
        var splitArgs = args[0].toLowerCase().trim().split(/ +/);

        if (splitArgs.length < 2)
        {
            input.channel.send("You need to provide a channel and a % threshold for when the monitor should turn to a " + discordUtils.symbols.ok + " for total damage.");
            return;
        }
        
        let channelId = discordUtils.cleanDiscordId(splitArgs[0]);
        
        var monitorChannel = client.channels.cache.get(channelId);
        
        if (!monitorChannel) {
            input.channel.send("You cannot do that to a channel that doesn't exist or is in another server.");
            return;
        }

        // check if the monitor is already installed in the channel
        var existingMonitor = await rancorUtils.getRancorMonitor(channelId);
        
        if (!existingMonitor) {
            input.channel.send("You do not have a " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor set up in " + splitArgs[0] + ". Install " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor with " + rancorUtils.getRancorOrShitPitWord(sfw).toLowerCase() + ".install.");
            return;
        }

        let threshold;
        try {
            // parseDamage throws errors for invalid damage
            threshold = rancorUtils.parseDamage(splitArgs[1], 0, 200);
        } catch (e) {
            input.channel.send(e.message);
            return;
        }

        // check if the player already has damage held in the phase and replace it if they do
        await database.db.none("UPDATE rancor_monitor SET threshold = $1 WHERE rancor_monitor_id = $2", [threshold, existingMonitor.rancor_monitor_id])

        existingMonitor.threshold = threshold;

        await rancorUtils.updateRancorMonitorMessage(existingMonitor, monitorChannel);
        input.channel.send("Damage threshold set to " + threshold + ".");
    }
}
