const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const rancorUtils = require("./utils.js");
const database = require("../../database");

module.exports = {
    name: "rancor.reset",
    aliases: ["shitpit.reset"],
    description: "Reset a WookieeBot Rancor/ShitPit monitor.",
    execute: async function(client, input, args, sfw) {
        var channelId, phase;

        if (!args) {
            input.channel.send("You need to provide the channel for the " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor.");
            return;
        }

        if (args.length >= 1)
        {
            var splitArgs = args[0].toLowerCase().trim().split(/ +/);
            channelId = discordUtils.cleanDiscordId(splitArgs[0]);
            if (splitArgs.length > 1)
            {
                phase = parseInt(splitArgs[1]);

                if (isNaN(phase) || phase < 1 || phase > 4)
                {
                    input.channel.send("Did you mean to reset a specific phase (1-4)?");
                    return;
                }
            }
        }

        await this.resetMonitor(client, input, channelId, phase, sfw);
    },
    resetMonitor: async function(client, input, channelId, phase, sfw)
    {
        if (!channelId) {
            input.channel.send("You need to provide the channel for the " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor.");
            return;
        }

        var monitorChannel = client.channels.cache.get(channelId);
        
        if (!monitorChannel) {
            input.channel.send("You cannot do that to a channel that doesn't exist or is in another server.");
            return;
        }

        var existingMonitor = await rancorUtils.getRancorMonitor(channelId);

        if (!existingMonitor) {
            input.channel.send("You do not have a " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor set up in " + args[0] + ". Install " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor with " + rancorUtils.getRancorOrShitPitWord(sfw).toLowerCase() + ".install.");
            return;
        }

        if (existingMonitor.static_message_snowflake)
        {
            (await monitorChannel.messages.fetch(existingMonitor.static_message_snowflake)).delete();
        }

        // reset rancor data
        if (!phase)
        {
            await database.db.none("DELETE FROM rancor_monitor_data WHERE rancor_monitor_id = $1", [existingMonitor.rancor_monitor_id]);
            await database.db.none("UPDATE rancor_monitor SET current_phase = 1, threshold = 100 WHERE rancor_monitor_id = $1", [existingMonitor.rancor_monitor_id]);
            existingMonitor.current_phase = 1;
        }
        else 
        {
            await database.db.none("DELETE FROM rancor_monitor_data WHERE rancor_monitor_id = $1 AND phase = $2", [existingMonitor.rancor_monitor_id, phase]);
            await database.db.none("UPDATE rancor_monitor SET current_phase = $1 WHERE rancor_monitor_id = $2", [phase, existingMonitor.rancor_monitor_id]);
            existingMonitor.current_phase = phase;
        }

        let currentRancorStatusEmbed = await rancorUtils.getRancorStatusEmbed(existingMonitor);

        const statusMessage = await monitorChannel.send(currentRancorStatusEmbed); 
        
        await database.db.none("UPDATE rancor_monitor SET static_message_snowflake = $1 WHERE rancor_monitor_id = $2", [statusMessage.id, existingMonitor.rancor_monitor_id]);

        let responseMessage;
        if (phase) responseMessage = "Monitor reset for Phase " + phase + " in " + discordUtils.makeChannelTaggable(channelId);
        else responseMessage = "Monitor reset in " + discordUtils.makeChannelTaggable(channelId);

        input.channel.send(responseMessage);
    }
}
