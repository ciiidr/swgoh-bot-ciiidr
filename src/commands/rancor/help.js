const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const rancorUtils = require("./utils.js");

module.exports = {
    name: "rancor.help",
    aliases: ["shitpit.help"],
    description: "Help for the WookieeBot Rancor/ShitPit command.",
    execute: async function(client, input, args, sfw) {

        let overviewField = {
            name: "Overview", 
            value: "The WookieeBot " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor uses a dedicated damage channel to track damage for each phase of The Pit (Challenge) and provides notifications when you are ready for your guild to post damage. For Discord servers that handle multiple guilds, you can set up any number of monitors, each with it's own dedicated channel.\r\n\r\n"  + 
                "Prerequisites:\r\n" + 
                "1. Create a dedicated channel for damage tracking.\r\n" +
                "2. Give WookieeBot permissions to read, write, and delete messages in that channel.\r\n" +
                "3. From another channel on the server, run **" + rancorUtils.getRancorOrShitPitWord(sfw) + ".install channel**, where channel is the dedicated channel from step 1.",
            inline: false
        };

        let commandsField = {
            name: "Commands",
            value: 
                "Run these commands from **outside** your dedicated damage channel." +
                "\r\n\r\n**^" + rancorUtils.getRancorOrShitPitWord(sfw) + ".install channel**: Install the monitor in a channel. Tag an existing, dedicated channel." +
                "\r\n\r\n**^" + rancorUtils.getRancorOrShitPitWord(sfw) + ".uninstall channel**: Stop monitoring the channel." +
                "\r\n\r\n**^" + rancorUtils.getRancorOrShitPitWord(sfw) + ".reset channel phase**: Reset a specific phase (1-4)." +
                "\r\n\r\n**^" + rancorUtils.getRancorOrShitPitWord(sfw) + ".reset channel**: Reset the monitor. This removes all held damage for all phases and should be used if something gets totally messed up." +
                "\r\n\r\n**^" + rancorUtils.getRancorOrShitPitWord(sfw) + ".next channel**: Advance to the next phase. When you finish the Rancor, use this command for a summary." +
                "\r\n\r\n**^" + rancorUtils.getRancorOrShitPitWord(sfw) + ".post channel**: Message all discord users holding damage to post. Users that had been holding damage can now post new damage for the same phase (until you use .next to move on)." +
                "\r\n\r\n**^" + rancorUtils.getRancorOrShitPitWord(sfw) + ".threshold (.t) channel %**: Set the threshold (0-200) on the monitor for showing a " + discordUtils.symbols.ok + "." +
                "\r\n\r\n**^" + rancorUtils.getRancorOrShitPitWord(sfw) + ".report channel**: Generate a CSV report of the current raid.",
            inline: false
        }

        let damageField = {
            name: "Submitting Damage",
            value: "Simply type the damage (in %, 0-100), into the channel in which the monitor is installed.  Really, that's it.  WookieeBot will send a DM if you type in something invalid.  Any damage message typed is immediately removed from the channel and added to the bot, which keeps both individual contributions and a running total per phase. A damage of 0 will remove the held damage for a player." + 
            "\r\n\r\nTo submit/update damage for another player (or for an alt), type **player damage** into the damage channel, where player is a name or discord tag.",
            inline: false
        }


        let embed = new MessageEmbed()
            .setTitle("WookieeBot Rancor Manager")
            .setColor(0xD2691E)
            .addFields(
                overviewField,
                commandsField,
                damageField
            );

        input.channel.send(embed);
    }

}
