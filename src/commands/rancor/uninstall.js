const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const rancorUtils = require("./utils.js");
const database = require("../../database");

module.exports = {
    name: "rancor.uninstall",
    aliases: ["shitpit.uninstall"],
    description: "Uninstall a WookieeBot Rancor/ShitPit monitor.",
    execute: async function(client, input, args, sfw) {
        if (!args || args.length == 0) {
            input.channel.send("Please provide the channel from which to uninstall the " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor.");
            return;
        }

        let channelId = discordUtils.cleanDiscordId(args[0]);
        
        var monitorChannel = client.channels.cache.get(channelId);
        
        if (!monitorChannel) {
            input.channel.send("You cannot do that to a channel that doesn't exist or is in another server.");
            return;
        }

        // check if the monitor is already installed in the channel
        var existingMonitor = await rancorUtils.getRancorMonitor(channelId);
        
        if (!existingMonitor) {
            input.channel.send("You do not have a " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor set up in " + args[0] + ". Install " + rancorUtils.getRancorOrShitPitWord(sfw) + " monitor with " + rancorUtils.getRancorOrShitPitWord(sfw).toLowerCase() + ".install.");
            return;
        }

        await database.db.none("DELETE FROM rancor_monitor WHERE channel = $1", channelId);

        input.channel.send(rancorUtils.getRancorOrShitPitWord(sfw) + " monitor uninstalled from " + args[0]);
    }
}
