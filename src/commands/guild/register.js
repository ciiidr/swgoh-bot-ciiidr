const guildUtils = require("../../utils/guild.js");
const discordUtils = require("../../utils/discord.js");
const playerUtils = require("../../utils/player.js");
const swapiUtils = require("../../utils/swapi.js");
const database = require("../../database");
const guildRefresh = require("./refresh.js");

module.exports = {
    name: "guild.register",
    description: "Register a guild for data tracking",
    execute: async function(client, input, args) {
        if (!args)
        {
            input.channel.send("Need to provide an ally code.");
            return;
        }
    
        var allycode = playerUtils.cleanAndVerifyStringAsAllyCode(args[0]);
            
        var guild = await guildUtils.getGuildData(allycode);
    
        var guildId = guild.id;
    
        const existingGuild = await database.db.one('SELECT COUNT(*) FROM guild WHERE guild_id = $1', [guildId], r => r.count > 0);
    
        if (existingGuild)
        {
            input.channel.send("Guild **" + guild.name + "** is already registered for tracking.");
            return;
        }

        // records to be updated:
        var guildInsertData = new Array();
    
        // declare ColumnSet once, and then reuse it:
        const guildCS = new database.pgp.helpers.ColumnSet(['guild_name', 'guild_id', 'allycode'], {table: 'guild'});
    
        guildInsertData.push({
            guild_name: guild.name,
            guild_id: guildId,
            allycode: allycode
        });
        
        const guildInsert = database.pgp.helpers.insert(guildInsertData, guildCS);
        
        // executing the query:
        await database.db.none(guildInsert);
    
        // refresh guild data
        var guildPlayersAndDataMessage = await guildRefresh.populateGuildPlayersAndData(input, guild);
    
        input.channel.send("Guild **" + guild.name + "** registered.\r\n" + guildPlayersAndDataMessage);
    }
    
}