const { MessageEmbed } = require("discord.js");
const database = require("../database");
const guildUtils = require("../utils/guild.js");
const discordUtils = require("../utils/discord.js");
const playerUtils = require("../utils/player.js");
const { ChartJSNodeCanvas } = require('chartjs-node-canvas');
const swapi = require("../utils/swapi.js");

const ONE_DAY_MILLIS = 1000*60*60*24;

const monthShortNames = 
[
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
];

const reportTypes = {
    "reek" : getReportReek,
    "gp" : getChartGpOverTimeForAllyCode,
    "gac" : getChartGacOverTimeForAllyCode,
    "gls" : getChartGLsForGuild,
    "mq" : getChartModQualityOverTimeForAllyCode,
    "payouts" : getChartGuildPayouts
};

module.exports = {
    name: "report",
    description: "Generate Reports",
    message: null,
    list: function(message) {
        
        let list = Object.keys(reportTypes);

        let embed = new MessageEmbed()
            .setTitle("Available Reports")
            .setColor(0xD2691E)
            .setDescription(list.sort().toString().split(',').join("\r\n"));

        message.channel.send(embed);
    },
    help: function(message) {
        let embed = new MessageEmbed()
            .setTitle("WookieeBot Reports")
            .setColor(0xD2691E)
            .setDescription(
                "gac: graph of gac lifetime score for a player (30 days)\r\n" +
                "gls: graph of GLs in the guild\r\n" +
                "gp: graph of gp growth for a player (30 days)\r\n" +
                "gp.t: graph of total gp growth for a player (30 days)\r\n" +
                "gp.s: graph of squad gp growth for a player (30 days)\r\n" +
                "gp.f: graph of fleet gp growth for a player (30 days)\r\n" +
                "mq: graph of mod quality growth for a player (Wookiee style, 30 days)\r\n" +
                "mq.dsr: graph of mod quality growth for a player (DSR style, 30 days)\r\n" +           
                "reek: list of guild members that have not beaten the reek\r\n\r\n" + 
                "payouts: chart of when guild members have payouts, and how many\r\n\r\n" + 

                "Examples:\r\n" +
                "^report reek 985736123\r\n" +
                "^report gp.s 985736123\r\n"
            );

        message.channel.send(embed);
    },
    execute: async function(client, message, args) {
        this.message = message;

        if (!args || !args.length)
        {
            this.help(message);
            return;
        }

        var splitArgs = args[0].toLowerCase().trim().split(/ +/);

        var allycode;

        if (splitArgs.length == 1)
        {
            // check if the caller is registered
            let user = await playerUtils.getUser(this.message.author.id);

            if (user && user.allycodes.length > 0) 
            {
                allycode = user.allycodes[0];
            }
            else
            {
                this.sendError("Error", "Need to be registered (use registeruser) or provide a report type and an ally code.");
                return;
            }
        } 
        else if (splitArgs.length != 2)
        {
            this.sendError("Error", "Need to provide a report type and an ally code.");
            return;
        }
        else
        {
            allycode = playerUtils.cleanAndVerifyStringAsAllyCode(splitArgs[1]);
        }
            
        var reportType = splitArgs[0].trim();

        var subreport = null;

        if (reportType.indexOf(".") > 0)
        {
            var splitReportType = reportType.split(/\./);
            if (splitReportType.length != 2)
            {
                this.sendError("Invalid Subreport Format", "Format is [report type].[subreport]. For example: report gg.kam allycode");
                return;
            }
            reportType = splitReportType[0];
            subreport = splitReportType[1];
        }

        if (reportTypes[reportType]) {
            var reportObj;

            if (subreport)
                reportObj = await reportTypes[reportType](allycode, subreport);
            else
                reportObj = await reportTypes[reportType](allycode);

            if (reportObj.imageBuffer)
            {
                this.message.channel.send({
                    files: [{
                        attachment: reportObj.imageBuffer,
                        name: reportObj.fileName
                    }]
                });
                return;
            } else {
                this.message.channel.send(reportObj);
                return;
            }
        }

        this.list(message);
    },
    sendError: function(title, errorMessage) {
        errorEmbed = new MessageEmbed().setTitle(title).setColor(0xaa0000).setDescription(errorMessage);
        this.message.channel.send(errorEmbed);
    }
}


function createImageReportResultObj(imageBuffer, fileName)
{
    return {
        imageBuffer: imageBuffer,
        fileName: fileName
    };
} 

async function getReportReek(allycode)
{
    
    var guildInfo = await guildUtils.getGuildIdAndAllyCodesForGuildByAllycode(allycode);
    const reekData = await database.db.any('SELECT allycode, player_name, reek_win FROM player_data_static WHERE reek_win = FALSE AND allycode LIKE ANY ($1) ORDER BY player_name ASC', [guildInfo.allycodes]);

    let desc = "";
    
    for (var d in reekData)
    {
        var dataRow = reekData[d];
        desc += dataRow.player_name + " (" + dataRow.allycode + ")\r\n";
    }

    var embed = new MessageEmbed()
        .setTitle("Guild members that have not beaten the Reek mission")
        .setColor(0xD2691E)
        .setDescription(desc);

    
    return embed;
}

async function getChartGLsForGuild(allycode)    
{
    const glData = await database.db.any(
        `SELECT   
         COUNT(*) FILTER (WHERE gl_rey = TRUE) AS rey_count, 
         COUNT(*) FILTER (WHERE gl_slkr = TRUE) AS slkr_count,
         COUNT(*) FILTER (WHERE gl_jml = TRUE) AS jml_count,
         COUNT(*) FILTER (WHERE gl_see = TRUE) AS see_count,
         COUNT(*) AS players_count
         FROM player_data_static WHERE allycode LIKE ANY 
         (SELECT allycode FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1))`, allycode
    );

    var data = [glData[0].rey_count, glData[0].slkr_count, glData[0].jml_count, glData[0].see_count];

    const width = 500;
    const height = 500;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            },
            afterDraw: function (chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.textBaseline = "middle";
                ctx.fillStyle = 'black'
                ctx.font = "15px Roboto";
                ctx.textAlign = "center";

                for (var li = 0; li < data.length; li++)
                {
                    var labelText = data[li];
                    var x = chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.x;
                    var y = Math.min(chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.y + 15, height - 40);
                    ctx.fillText(labelText, x, y);
                }
            }
          });
    };

    var totalGls = (parseInt(glData[0].rey_count) + parseInt(glData[0].slkr_count) + parseInt(glData[0].jml_count) + parseInt(glData[0].see_count));
    var chartTitle = "Total GLs for guild: " + totalGls.toLocaleString('en');

    const configuration = {
        type: 'bar',
        data: {
            labels: ['Rey', 'SL Kylo Ren', 'JM Luke', 'Sith Emperor'],
            datasets: [{
                label: 'Count of Galactic Legends',
                data: data,
                backgroundColor: [
                    'rgba(54, 162, 235, 0.7)',
                    'rgba(255, 99, 132, 0.7)',
                    'rgba(255, 206, 86, 0.7)',
                    'rgba(75, 192, 192, 0.7)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            },
            title: {
                display: true,
                align: 'center',
                text: chartTitle,
                position: 'bottom'
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-gls-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGacOverTimeForAllyCode(allycode)
{
    var playerData = await playerUtils.getAllPlayerDataByAllyCode(allycode);

    var labels = new Array();
    var data = new Array();

    if (playerData.length == 0)
        throw "No data for allycode " + allycode;

    playerData.sort((a,b) => a.timestamp - b.timestamp);

    // grab their most recent recorded name
    var playerName = playerData[playerData.length - 1].player_name;
    
    var gacAveragePerDay = "Not enough data.";
    
    if (playerData.length > 1) {
        var daysOfData = Math.round((playerData[playerData.length-1].timestamp.getTime() - playerData[0].timestamp.getTime())/ONE_DAY_MILLIS);
        var gacChange = playerData[playerData.length-1].gac_lifetime_score - playerData[0].gac_lifetime_score;
        gacAveragePerDay = Math.round(gacChange / daysOfData);   
    }

    for (var i = 0; i < playerData.length; i++) {
        var ts = playerData[i].timestamp;
        var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
        labels.push(label);
        data.push(playerData[i].gac_lifetime_score);
    }

    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };

    var chartTitle = "Average GAC Lifetime increase per day: " + gacAveragePerDay.toLocaleString('en');

    const configuration = {
        type: 'line',
        data: {
            datasets: [{
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                cubicInterpolationMode: 'monotone',
                label: "GAC Lifetime Score over time for " + playerName + " (" + allycode + ")",
                data: data
            }],
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                text: chartTitle
            }
        }
    };
    
    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-gac-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGpOverTimeForAllyCode(allycode, type = null)
{
    var playerData = await playerUtils.getAllPlayerDataByAllyCode(allycode);

    var labels = new Array();
    var gpData = new Array();
    var gpSquadData = new Array();
    var gpFleetData = new Array();

    if (playerData.length == 0)
        throw "No data for allycode " + allycode;

    playerData.sort((a,b) => a.timestamp - b.timestamp);

    // grab their most recent recorded name
    var playerName = playerData[playerData.length - 1].player_name;

    var gpAveragePerDay = "Not enough data.";
    var gpSquadAveragePerDay = "Not enough data.";
    var gpFleetAveragePerDay = "Not enough data.";
    
    if (playerData.length > 1) {
        var daysOfData = Math.round((playerData[playerData.length-1].timestamp.getTime() - playerData[0].timestamp.getTime())/ONE_DAY_MILLIS);
        var gpChange = playerData[playerData.length-1].gp - playerData[0].gp;
        gpAveragePerDay = Math.round(gpChange / daysOfData);

        var gpSquadChange = playerData[playerData.length-1].gp_squad - playerData[0].gp_squad;
        gpSquadAveragePerDay = Math.round(gpSquadChange / daysOfData);   

        var gpFleetChange = playerData[playerData.length-1].gp_fleet - playerData[0].gp_fleet;
        gpFleetAveragePerDay = Math.round(gpFleetChange / daysOfData);   
    }

    for (var i = 0; i < playerData.length; i++) {
        var ts = playerData[i].timestamp;
        var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
        labels.push(label);
        gpData.push(playerData[i].gp);
        gpSquadData.push(playerData[i].gp_squad);
        gpFleetData.push(playerData[i].gp_fleet);
    }

    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };

    var datasets = new Array();
    if (!type || type == "t")
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(54, 162, 235, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Total GP",
            data: gpData
        });
    }
    if (!type || type == "s")
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(14, 180, 5, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Squad GP",
            data: gpSquadData
        });
    }
    if (!type || type == "f") 
    {
        datasets.push({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(180, 25, 50, 0.5)',
            cubicInterpolationMode: 'monotone',
            label: "Fleet GP",
            data: gpFleetData
        });
    }

    var line1 = "GP over time for " + playerName + " (" + allycode + ")";
    if (type == "s") line1 = "Squad " + line1; 
    if (type == "f") line1 = "Fleet " + line1; 

    var titleText = [
        line1,
        "Average GP increase per day: " + gpAveragePerDay.toLocaleString('en'),
        "Average Squad GP increase per day: " + gpSquadAveragePerDay.toLocaleString('en'),
        "Average Fleet GP increase per day: " + gpFleetAveragePerDay.toLocaleString('en')
    ];

    const configuration = {
        type: 'line',
        data: {
            datasets: datasets,
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                text: titleText
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-gp-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartModQualityOverTimeForAllyCode(allycode, type = "")
{
    type = type ? type.toLowerCase() : "";
    var playerData = await playerUtils.getAllPlayerDataByAllyCode(allycode);

    var labels = new Array();
    var data = new Array();

    if (playerData.length == 0)
        throw "No data for allycode " + allycode;

    playerData.sort((a,b) => a.timestamp - b.timestamp);

    // grab their most recent recorded name
    var playerName = playerData[playerData.length - 1].player_name;

    for (var i = 0; i < playerData.length; i++) {
        var ts = playerData[i].timestamp;
        var label = monthShortNames[ts.getUTCMonth()] + " " + ts.getUTCDate();
        labels.push(label);
        if (type == "dsr")
            data.push(playerData[i].mod_quality);
        else
            data.push(playerData[i].mod_quality2)
    }

    const width = 600;
    const height = 300;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            }
          });
    };

    var chartTitle = [
        "Mod Quality over time for " + playerName + " (" + allycode + "),"
    ];

    if (type == "dsr")
        chartTitle.push("calculated as (Number of +15 speeds) / (Squad GP / 100,000)");
    else
       chartTitle.push("calculated as (Num. 15+20+25 speeds + 0.5*Num. 6-dot) / (Squad GP / 100,000)");

    const configuration = {
        type: 'line',
        data: {
            datasets: [{
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                cubicInterpolationMode: 'monotone',
                label: "Mod Quality",
                data: data
            }],
            labels: labels
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                text: chartTitle
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-mq-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}

async function getChartGuildPayouts(allycode, type = "")
{
    var guildName = null;
    var payoutsData = null;
    var guildData = await database.db.any("select guild_name from guild where guild_id = (select guild_id from guild_players where allycode = $1)", allycode);

    if (guildData && guildData[0]) {
        guildName = guildData[0].guild_name;
        var payoutsData = await database.db.any(
            `select distinct payout_utc_offset_minutes, count(payout_utc_offset_minutes)
            FROM player_data_static WHERE allycode LIKE ANY 
            (SELECT allycode FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1))
            group by payout_utc_offset_minutes
            order by payout_utc_offset_minutes asc`, allycode
        );
    }

    if (payoutsData == null || payoutsData.length == 0)
    {
        // guild not registered in db, so load from API
        var guildData = await guildUtils.getGuildData(allycode);
        
        var memberAllyCodes = new Array();
        for (var m = 0; m < guildData.roster.length; m++)
        {
            guildName = guildData.name;
            var memberAllyCode = guildData.roster[m].allyCode;
            memberAllyCodes.push(memberAllyCode);
        }

        var playerPayouts = await swapi.fetchPlayer(
            { 
                allycodes: memberAllyCodes,
                project : 
                {
                    "poUTCOffsetMinutes": 1
                }
            }
        );

        playerPayouts.result.sort((a, b) => a.poUTCOffsetMinutes - b.poUTCOffsetMinutes);
        payoutsData = new Array();
        var lastPayoutData = null;
        for (var p = 0; p < playerPayouts.result.length; p++)
        {
            var poData = playerPayouts.result[p].poUTCOffsetMinutes;
            if (lastPayoutData == null || lastPayoutData.payout_utc_offset_minutes != poData)
            {
                lastPayoutData = { payout_utc_offset_minutes: poData, count: 1 }
                payoutsData.push(lastPayoutData);
            } else {
                lastPayoutData.count++;
            }
        }
    }

    var data = new Array();
    var labels = new Array();
    var backgroundColors = new Array();
    var borderColors = new Array();

    const colorLimits = {
        rMin: 20,
        rMax: 100,
        gMin: 0,
        gMax: 230,
        bMin: 100,
        bMax: 200
    }
    for (var i = 0; i < payoutsData.length; i++)
    {
        data.push(payoutsData[i].count);

        var offsetHours = payoutsData[i].payout_utc_offset_minutes / 60;
        var label = "UTC " + (offsetHours >= 0 ? "+" : "-") + Math.abs(offsetHours);
        labels.push(label);

        var baseColorR = Math.floor((colorLimits.rMax - colorLimits.rMin) / payoutsData.length) * i + colorLimits.rMin;
        var baseColorG = Math.floor((colorLimits.gMax - colorLimits.gMin) / payoutsData.length) * i + colorLimits.gMin;
        var baseColorB = Math.floor((colorLimits.bMax - colorLimits.bMin) / payoutsData.length) * i + colorLimits.bMin;
        backgroundColors.push("rgba(" + baseColorR + "," + baseColorG + "," + baseColorB + ", 0.7)");
        borderColors.push("rgba(" + baseColorR + "," + baseColorG + "," + baseColorB + ", 1)");
        
    }

    const width = 40*data.length;
    const height = 500;
    const chartCallback = (ChartJS) => {
        ChartJS.defaults.global.responsive = true;
        ChartJS.defaults.global.maintainAspectRatio = false;

        ChartJS.plugins.register({
            beforeDraw: function(chartInstance) {
              var ctx = chartInstance.chart.ctx;
              ctx.fillStyle = "white";
              ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
            },
            afterDraw: function (chartInstance) {
                var ctx = chartInstance.chart.ctx;
                ctx.textBaseline = "middle";
                ctx.fillStyle = 'black'
                ctx.font = "15px Roboto";
                ctx.textAlign = "center";

                for (var li = 0; li < data.length; li++)
                {
                    var labelText = data[li];
                    var x = chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.x;
                    var y = Math.min(chartInstance.chart.controller.data.datasets[0]._meta[0].data[li]._view.y + 15, height - 40);
                    ctx.fillText(labelText, x, y);
                }
            }
          });
    };

    const configuration = {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{

                label: 'Count',
                data: data,
                backgroundColor: backgroundColors,
                borderColor: borderColors,
                borderWidth: 1
            }]
        },
        options: {
            title: {
                display: true,
                align: 'center',
                position: 'bottom',
                text: "Payouts for " + guildName + ", grouped by time (UTC)"
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            }
        }
    };

    const chartJSNodeCanvas = new ChartJSNodeCanvas({width, height, chartCallback: chartCallback});

    const imageBuffer = await chartJSNodeCanvas.renderToBuffer(configuration);    

    var fileName = "chart-guild-payouts-" + allycode + "-" + Date.now() + ".png";

    return createImageReportResultObj(imageBuffer, fileName);
}