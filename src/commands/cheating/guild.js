const { MessageEmbed } = require("discord.js");
const discordUtils = require("../../utils/discord.js");
const cheatingUtils = require("./utils.js");
const playerUtils = require("../../utils/player.js");
const guildUtils = require("../../utils/guild.js");
const swapi = require("../../utils/swapi.js");
const jp = require('jsonpath');
const cheating_data = require("../../data/cheating_data.json");

module.exports = {
    name: "cheating.guild",
    description: "Check all players in a guild for potential cheating.",
    execute: async function(client, input, args)
    {
        if (!args || !args.length) {
            return cheatingUtils.createErrorEmbed("Rwww. ('No.').", "You didn't give me an ally code!");
        }

        const initialResponseMessage = await input.channel.send("Cheat check underway for guild...");


        var allycode = playerUtils.cleanAndVerifyStringAsAllyCode(args[0]);

        var guild = await guildUtils.getGuildData(allycode);

        var guildAllyCodes = new Array();

        for (var m = 0; m < guild.roster.length; m++)
        {
            var memberAllyCode = guild.roster[m].allyCode;
        
            guildAllyCodes.push(memberAllyCode);
        }

        initialResponseMessage.edit("Found guild **" + guild.name + "**.  Checking " + guildAllyCodes.length + " players, please wait...");

        var unitsList = await playerUtils.getUnitsList();
        var foundCheater = false;
        var embedDescription = "Here are the players that have a high likelihood of cheating:\r\n";
        embedDescription += "```";
        
        
        let payload = {
            allycodes: guildAllyCodes, 
            language: "eng_us",
            project: {
                name: 1,
                allyCode: 1,
                roster: {
                    nameKey: 1,
                    rarity: 1,
                    gear: 1,
                    skills: 1
                }
            }
        };

        let { result, error, warning } = await swapi.swapi.fetchPlayer(payload);

        for (var playerIndex = 0; playerIndex < result.length; playerIndex++)
        {
            var player = result[playerIndex];

            for (var checkCharDisplayName in cheating_data)
            {
                var checkChar = cheating_data[checkCharDisplayName].character_name;
                
                // confirm they have the character activated
                var rareCharQuery = jp.query(player, "$..roster[?(@.nameKey=='" + checkChar + "')]");
                if (rareCharQuery.length == 0 || rareCharQuery[0].rarity != 7) continue;

                let results = await cheatingUtils.checkRequirements(player, cheating_data[checkCharDisplayName], unitsList);

                // something suspicious with this player.  Add to the list and move to the next player
                // otherwise, check the next character
                if (results.likelihood >= cheatingUtils.criticalThreshold)
                {
                    foundCheater = true;
                    embedDescription += player.allyCode + " (" + player.name + ")\r\n"

                    break;
                }
            }
        }

        if (foundCheater)
            embedDescription += "```";
        else {
            embedDescription = "No players in **" + guild.name + "** are likely to have cheated at checked events.";
        }

        var embedToSend = new MessageEmbed()
            .setTitle("Cheat check results for guild **" + guild.name + "**:")
            .setDescription(embedDescription)
            .setColor(0xD2691E);

        input.channel.send(embedToSend);
    }
}