const jp = require('jsonpath');
const { MessageEmbed } = require("discord.js");

module.exports = {
    criticalThreshold: 60,
    warningThreshold: 20,
    likelihoodSymbol: function(likelihood)
    {
        if (likelihood >= this.criticalThreshold) {
            return ":rotating_light:";
        } else if (likelihood >= this.warningThreshold) {
            return ":thinking:";
        } else
        {
            return ":white_check_mark:";
        }
    },
    checkRequirements: async function(player, characterObj, unitsList)
    {
        var numCharacters = characterObj.requirements.num_characters;

        var faction = characterObj.requirements.faction;
        var faction_units = [];

        if (faction)
        {
            faction_units = unitsList.filter((u) => 
                u.categoryIdList.includes(faction) &&
                !characterObj.requirements.characters.find((c) => c.name == u.nameKey)
                );
        }

        var resultsSet = [];
        
        for (var charIndex in characterObj.requirements.characters)
        {
            var characterRequirement = characterObj.requirements.characters[charIndex];

            var resultObj = this.checkCharacter(player, characterRequirement);

            if (resultObj != null && resultObj.character.stars >= 7) 
                resultsSet.push(resultObj);
        }

        for (var fuix = 0; fuix < faction_units.length; fuix++)
        {
            var characterRequirement = characterObj.requirements.faction_characters_default;

            var resultObj = this.checkCharacter(player, characterRequirement, faction_units[fuix].nameKey);

            if (resultObj != null && resultObj.character.stars >= 7) 
                resultsSet.push(resultObj);
        }

        // sort in numeric order
        resultsSet = resultsSet.sort((a, b) => b.impact - a.impact);

        
        var results = {
            characters: [],
            likelihood: 100
        };
        
        // only look at the first numCharacters impacts on likelihood, since those are the characters that are most
        // likely to have been used in the event
        for (var ix = 0; ix < numCharacters && ix < resultsSet.length; ix++)
        {
            results.likelihood -= resultsSet[ix].impact;
            results.characters.push(resultsSet[ix].character);
        }

        // can't be less than a 0% chance or above 100%
        results.likelihood = Math.min(Math.max(0, results.likelihood), 100);

        //console.log(results);

        return results;
    },
    checkCharacter: function(player, characterRequirement, characterName)
    {
        if (!characterName)
            characterName = characterRequirement.name;

        var resultObj = {
            impact: 0,
            character: {
                characterName: characterName,
                stars: 0,
                gear: 0,
                zetas: 0
            }
        };

        var charQueryResults = [];

        try {
            charQueryResults = jp.query(player, "$..roster[?(@.nameKey=='" + characterName + "')]");
        } catch(error) {
            console.error(error);
        }

        if (charQueryResults.length > 0) 
        {
            var actualChar = charQueryResults[0];

            if (actualChar.rarity < 7)
                return null;

            resultObj.character.stars = actualChar.rarity;
            resultObj.character.gear = actualChar.gear;
            
            // find out how many zetas the player put on the character
            var zetaQueryResults = jp.query(actualChar, "$..skills[?(@.isZeta==true && @.tier == 8)]");
            resultObj.character.zetas = zetaQueryResults.length;
            
            resultObj.impact = characterRequirement.gear_scale[resultObj.character.gear] 
                + characterRequirement.zeta_scale[resultObj.character.zetas];

        }

        return resultObj;
    },
    
    createErrorEmbed: function(title, errorMessage) {
        errorEmbed = new MessageEmbed().setTitle(title).setColor(0xaa0000).setDescription(errorMessage);
        return errorEmbed;
    }
};