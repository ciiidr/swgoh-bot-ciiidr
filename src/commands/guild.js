const discordUtils = require("../utils/discord.js");
const guildRegister = require("./guild/register.js");
const guildRefresh = require("./guild/refresh.js");

module.exports = {
    name: "guild",
    description: "Gather data for the guild",
    execute: async function(client, input, args) {

        // for now, bot admin only
        if (!discordUtils.isBotOwner(input.author.id)) return;
        
        let cmd = discordUtils.splitCommandInput(input);

        switch (cmd[1])
        {
            case "register":
                await guildRegister.execute(client, input, args);
                break;
            case "refresh":
                await guildRefresh.execute(client, input, args);
                break;
            default: break;
        }
    }
}