const ApiSwgohHelp = require('api-swgoh-help');

module.exports.swapi = new ApiSwgohHelp({
    "username":process.env.API_USERNAME,
    "password":process.env.API_PASSWORD,
});