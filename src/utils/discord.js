const Discord = require('discord.js');

module.exports = {
    prefix: "^",
    symbols : {
        ok: ":white_check_mark:",
        pass: ":white_check_mark:",
        bad: ":warning:",
        warning: ":warning:",
        fail: ":x:",
        smallBlueDiamond: ":small_blue_diamond:",
        question: ":grey_question:"
    },
    cleanDiscordId: function (id) {
        if (!id) return null;
        return id.replace(/[^0-9]/g, "");
    },
    isBotOwner: function(userId) {
        let cleanId = this.cleanDiscordId(userId);
        return cleanId == "276185061970149377" || cleanId == "276146474268753920";
    },
    isDiscordTag: function(tag) {
        return /^<(#|@!)[0-9]+>$/g.test(tag);
    },
    isDiscordId: function (id) {
        return /^[0-9]+$/g.test(id);
    },
    makeChannelTaggable: function(id){
        if (!id || id[0] == '<' || !this.isDiscordId(id)) return id;
        return "<#" + id + ">";
    },
    makeIdTaggable: function(id) {
        if (!id || id[0] == '<' || !this.isDiscordId(id)) return id;
        return "<@!" + id + ">";
    },
    splitCommandInput: function(input) {
        if (!input || !input.content) return null;
        let message = input.content;
        if (!message.startsWith(this.prefix)) {
            message = this.prefix+message.split(this.prefix)[1];
        }

        var cmd;

        let spaceIx = message.indexOf(" ");
        if (spaceIx > 0)
        {
            cmd = message.slice(this.prefix.length, spaceIx).toLowerCase();
        } else {
            cmd = message.slice(this.prefix.length).toLowerCase();
        }
        
        return cmd.split(".");
    }

    // TODO: The following functions are built into discord.js and are no longer needed. Please delete after review.
    // updateDiscordMessage: function (bot, channelId, messageId, newMessage) { return updateDiscordMessage(bot, channelId, messageId, newMessage); },
    // reactToDiscordMessage: function (bot, channelId, messageId, reaction) { return reactToDiscordMessage(bot, channelId, messageId, reaction); },
    // removeReactionsToDiscordMessage: function (bot, channelId, messageId) { return removeReactionsToDiscordMessage(bot, channelId, messageId); },

}