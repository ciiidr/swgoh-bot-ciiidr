const swapi = require('./swapi');
const database = require('../database');

module.exports = {

    getGuildData: async function(allycode) {
        let payload = {
            allycode:allycode,
            language: "eng_us"
        };

        let { result, error, warning } = await swapi.swapi.fetchGuild(payload);

        if (!result || !result[0]) throw "No guild found for ally code " + allycode;

        var guild = result[0];

        return guild;
    },
    getGuildIdAndAllyCodesForGuildByAllycode: async function (allycode) {
        const guildPlayerData = await database.db.any(
            'SELECT allycode,guild_id FROM guild_players WHERE guild_id = (SELECT guild_id FROM guild_players WHERE allycode = $1)'
            , [allycode]);

        return { allycodes: guildPlayerData.map(d => d.allycode), guildId: guildPlayerData[0].guild_id };
    }
}