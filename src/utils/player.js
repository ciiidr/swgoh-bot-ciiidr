const Discord = require('./discord');
const swapi = require('./swapi');
const database = require('../database'); //TODO: Should move this to utils?

module.exports = {
    getUser: async function(userId) {
        const cleanUserId = Discord.cleanDiscordId(userId);
        const userRoles = await database.db.any("SELECT * FROM user_guild_role WHERE discord_id = $1", [cleanUserId]);

        const userObj = {
            userId: cleanUserId,
            isBotOwner: Discord.isBotOwner(cleanUserId),
            allycodes: new Array(),
            guilds: new Array()
        }

        if (userRoles) {
            for (let ur = 0; ur < userRoles.length; ur++)
            {
                userObj.guilds.push({
                    guildId: userRoles[ur].guild_id,
                    guildAdmin: userRoles[ur].guild_admin,
                    guildBotAdmin: userRoles[ur].guild_bot_admin
                });
            }
        }

        const userRegs = await database.db.any('SELECT allycode FROM user_registration WHERE discord_id = $1', [cleanUserId]);
        if (userRegs)
        {
            for (let r = 0; r < userRegs.length; r++)
            {
                userObj.allycodes.push(userRegs[r].allycode);
            }
        }

        return userObj;
    },
    getUserRegistration: async function(userId, allycode) {
        const cleanUserId = Discord.cleanDiscordId(userId);
        return await database.db.oneOrNone('SELECT * FROM user_registration WHERE discord_id = $1 AND allycode = $2', [cleanUserId, allycode]);
    },
    getUnitsList: async function () {
        let data = await swapi.swapi.fetchData(
            {
                collection : "unitsList",
                language: "eng_us",
                match :
                    {
                        "rarity":7,
                        "obtainable": true,
                        "obtainableTime": 0,
                        "combatType": 1 // characters only
                    },
                project :
                    {
                        "nameKey": 1,
                        "baseId": 1,
                        "descKey": 1,
                        "categoryIdList": 1
                    }
            }
        );

        return data.result;
    },
    getAllPlayerDataByAllyCode: async function (allycode) {
        return await database.db.any('SELECT * FROM player_data WHERE allycode = $1 ORDER BY timestamp DESC LIMIT 30', [allycode]);
    },
    getAllGoalDataByAllyCode: async function (allycode) {
        return await database.db.any('SELECT * FROM goal_player_status WHERE allycode = $1 ORDER BY timestamp DESC LIMIT 30', [allycode]);
    },
    cleanAndVerifyStringAsAllyCode: function (arg) {
        const allycode = arg.replace(/-/g, "").trim();

        if (allycode.length != 9 || isNaN(allycode))
            throw "Invalid ally code (" + allycode + ").  Needs to be 9 digits."

        return allycode;
    }
}