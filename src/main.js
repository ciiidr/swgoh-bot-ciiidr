const fs = require('fs');
const Discord = require('discord.js');
const discordUtils = require('./utils/discord.js');
var logger = require('winston');
require('dotenv').config()

// Initiate the Discord Bot
const client = new Discord.Client();
client.login(process.env.BOT_TOKEN);
client.on('ready', () => {
    logger.info("Logged in as "+client.user.tag);
});

// Set up dynamic commands
client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync(__dirname + '/commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
    const command = require('./commands/'+file);
    client.commands.set(command.name, command);
}

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';

const rancor = require('./commands/rancor.js');
const rancorUtils = require('./commands/rancor/utils.js');

// Initiate receiver and process
client.on('message', async function (input) {
    
    // special handling for rancor monitor
    let rmc = await rancorUtils.getRancorMonitor(input.channel.id);
    if (rmc) {
        rancor.activity(client, input, rmc);
        return;
    }

    let message = input.content;
    if (!message.startsWith(discordUtils.prefix)) {

        if (message.indexOf(discordUtils.prefix) <= 0) return;

        message = discordUtils.prefix+message.split(discordUtils.prefix)[1];
    }

    if (message.length <= 1) return;

    let cmd, args;

    let spaceIx = message.indexOf(" ");
    if (spaceIx > 0)
    {
        cmd = message.slice(discordUtils.prefix.length, spaceIx).toLowerCase();
        args = message.slice(discordUtils.prefix.length + cmd.length).trimLeft().toLowerCase().split(/ +-/);
    } else {
        cmd = message.slice(discordUtils.prefix.length).toLowerCase();
    }

    cmd = cmd.split(".");
    
    const command = client.commands.get(cmd[0]) || client.commands.find(c => c.aliases && c.aliases.includes(cmd[0]));

    if(!command) {
        const memes = client.commands.get('memes');
        const wookiee = client.commands.get('wookiee');
        let msg = memes.check(cmd[0]) ? memes.post(input, cmd[0]) : wookiee.talk(input);
        return;
    }

    try {
        command.execute(client, input, args);
    } catch (error) {
        console.error(error);
        await input.reply('This command is not working correctly');
    }
});

