var Discord = require('discord.io');
var logger = require('winston');
require('dotenv').config()
const modules = require('./modules/index');
var database = require("../database");

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';

// Initialize Discord Bot

var bot = new Discord.Client({
    token: process.env.BOT_TOKEN,
    autorun: true
});

const prefix = "^";

process.on('uncaughtException', err => {
    logger.error('There was an uncaught error: ' + err)
    process.exit(1) //mandatory (as per the Node.js docs)
})

bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
});

//Listen for messages
bot.on('message', async function (user, userID, channelID, message, evt) {
    
    var callerInfo = {
        channelId: channelID,
        userId: userID,
        userName: user,
        bot: bot,
        messageId: null,
        evt: evt
    };

    let rmc = await modules.shitpit.getRancorMonitor(channelID);
    if (rmc) {
        await modules.shitpit.activity(rmc, message, callerInfo);
        return;
    }

    if (!message.startsWith(prefix)) {
        if (message.indexOf(prefix) <= 0) {
            return;
        }

        message = prefix+message.split(prefix)[1];
    }

    if (message.length <= 1) return;
    
    var cmd, args;

    var spaceIx = message.indexOf(" ");
    if (spaceIx > 0)
    {
        cmd = message.slice(prefix.length, spaceIx).toLowerCase();
        args = message.slice(prefix.length + cmd.length).trimLeft().split(/ +-/);
    } else {
        cmd = message.slice(prefix.length).toLowerCase();
    }

    switch(cmd) {
        // !ping
        case 'ping':
            logger.info("Received ping at " + new Date());
            bot.sendMessage({
                to: channelID,
                message: 'Wyaaaaaa. Ruh ruh. ("Hello. How are you?")'
            });
            break;
        case 'mod':
            let response = await modules.player.parsePlayer(args);
            console.log(response);
            bot.sendMessage({
                to: channelID,
                embed: response,
                typing: true
            });
            break;
        case 'cheating':
            bot.sendMessage({
                to: channelID,
                message: "Cheat check underway..."
            });

            await callAndRespondEmbed(modules.cheating.parsePlayer, args, callerInfo);
            break;
        case 'cheating.guild':
            var cheatingGuildMessageId;
            bot.sendMessage({
                to: channelID,
                message: "Cheat check underway for guild..."
            }, async function(err, res) { 
                cheatingGuildMessageId = res.id; 
                callerInfo.messageId = cheatingGuildMessageId;

                await callAndRespondEmbed(modules.cheating.cheatCheckGuild, args, callerInfo);
            });
            break;
        case 'rancor.help':
        case 'shitpit.help':
            await callAndRespondEmbed(modules.shitpit.help, args, callerInfo, cmd.startsWith("r") ? true : null);
            break;
        case 'rancor.install':
        case 'shitpit.install':
            await callAndRespondMessage(modules.shitpit.install, args, callerInfo, cmd.startsWith("r") ? true : null);
            break;
        case 'rancor.uninstall':
        case 'shitpit.uninstall':
            await callAndRespondMessage(modules.shitpit.uninstall, args, callerInfo, cmd.startsWith("r") ? true : null);
            break;
        case 'rancor.reset':
        case 'shitpit.reset':
            await callAndRespondMessage(modules.shitpit.reset, args, callerInfo, cmd.startsWith("r") ? true : null);
            break;
        case 'rancor.next':
        case 'shitpit.next':
            await callAndRespondMessage(modules.shitpit.nextPhase, args, callerInfo, cmd.startsWith("r") ? true : null);
            break;
        case 'rancor.post':
        case 'shitpit.post':
            await callAndRespondMessage(modules.shitpit.postDamage, args, callerInfo, cmd.startsWith("r") ? true : null);
            break;
        case 'rancor.threshold':
        case 'shitpit.threshold':
        case 'rancor.t':
        case 'shitpit.t':
            await callAndRespondMessage(modules.shitpit.setThreshold, args, callerInfo, cmd.startsWith("r") ? true : null);
            break;
        case 'rancor.report':
        case 'shitpit.report':
            await callAndRespondMessage(modules.shitpit.sendReport, args, callerInfo, cmd.startsWith("r") ? true : null);
            break;
        // case 'rancor.update':
        // case 'shitpit.update':
        //     await callAndRespondMessage(modules.shitpit.updateDamage, args, callerInfo, cmd.startsWith("r") ? true : null);
        //     break;
        case 'invite':
            bot.createDMChannel(userID, (err, res) => {
                if (res) {
                    bot.sendMessage({
                        to: res.recipients[0].id,
                        message: "Invite WookieeBot to your server:\r\nhttps://discordapp.com/api/oauth2/authorize?client_id=537868729732562944&permissions=0&scope=bot"
                    }, errorFunc);
                } else {
                    logger.info(err);
                }
            });
            break;
        case 'registeruser':
            await callAndRespondMessage(modules.users.registerUser, args, callerInfo);
            break;
        case 'setguildadmin':
            await callAndRespondMessage(modules.users.setGuildAdmin, args, callerInfo);
            break;
        case 'registerguild':
            await callAndRespondMessage(modules.guildData.registerGuild, args, callerInfo);
            break;
        case 'updateguild':
            await callAndRespondMessage(modules.guildData.refreshGuildData, args, callerInfo);
            break;
        case 'report':
            var reportObj;
            try {
                reportObj = await modules.reports.getReport(args, callerInfo);
            } 
            catch (e)
            {
                bot.sendMessage({
                    to: channelID,
                    message: e
                });
                break;
            }

            if (reportObj.imageBuffer)
            {
                bot.uploadFile({
                    message: reportObj.message,
                    file: reportObj.imageBuffer,
                    to: channelID,
                    filename: reportObj.fileName
                },errorFunc);
            } else {
                bot.sendMessage({
                    to: channelID,
                    embed: reportObj,
                    typing: true
                });
            }
            break;
        case 'setgoal':
            await callAndRespondMessage(modules.goals.startTrackingGoalSetForGuild, args, callerInfo);
            break;
        case 'remgoal':
            await callAndRespondMessage(modules.goals.stopTrackingGoalSetForGuild, args, callerInfo);
            break;
        case 'updategoals':
            await callAndRespondMessage(modules.goals.updateGuildStatusForGoals, args, callerInfo);
            break;
        // case 'troopers':
        // case 'starck':
        //     let speeds = await trooperSpeeds.getTrooperSpeeds(args);
        //     await bot.sendMessage({
        //        to: channelID,
        //        embed: speeds
        //     });
        //     break;

        case 'help':
            await callAndRespondEmbed(modules.help.getHelp, args, callerInfo);
            break;
        case 'guide':
            var guide;
            try {
                guide = await modules.guides.getGuide(args);
                
                bot.uploadFile({
                    file: guide,
                    to: channelID
                },errorFunc);
            } 
            catch (e)
            {
                bot.sendMessage({
                    to: channelID,
                    message: e
                });
            }
            
            break;
        case 'fact':
            await callAndRespondEmbed(modules.trivia.fact, args, callerInfo);
            break;
        case 'memes':
            let list = modules.meme.list();
            bot.sendMessage({
                to: channelID,
                embed: list
            });
            break;
        default :
            let msg = modules.meme.check(cmd) ? modules.meme.post(cmd) : modules.wookiee.talk();

            bot.sendMessage({
                to: channelID,
                message: msg
            })

    }
});

async function errorFunc(error, response)
{
    logger.info(error);
    logger.info(response);
}

async function callAndRespondMessage(func, args, callerInfo, data)
{
    var response = await callBotFunc(func, args, callerInfo, data);
    
    if (!response) return;

    callerInfo.bot.sendMessage({
        to:callerInfo.channelId,
        message:response
    });
}

async function callAndRespondEmbed(func, args, callerInfo, data)
{
    var response = await callBotFunc(func, args, callerInfo, data);
    
    if (!response) return;

    callerInfo.bot.sendMessage({
        to:callerInfo.channelId,
        embed:response
    });
}

async function callBotFunc(func, args, callerInfo, data)
{
    var response;
    try {
        response = await func(args, callerInfo, data);
    } catch (e) {
        response = e;
    }

    return response;
}

async function returnData (args, callback) {
    return await callback(args);
}