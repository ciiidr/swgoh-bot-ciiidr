const { MessageEmbed } = require("discord.js");
const cheating_data = require("../../data/cheating_data.json");
const database = require("../../database");
const playerUtils = require("../../utils/player.js");
const guildUtils = require("../../utils/guild.js");
const discordUtils = require("../../utils/discord.js");
const swapi = require("../../utils/swapi.js");
const jp = require('jsonpath');

const warningThreshold = 20;
const criticalThreshold = 60;

module.exports = {
    name: "cheating",
    description: "Check players for potential cheating",
    message: null,
    execute: function(client, message, args) {
        this.singlePlayer(message, args, null);
    },
    singlePlayer: async function(message, args, callerInfo) {

        console.log(playerUtils.getUnitsList());
        message.channel.send("Cheat check underway...");

        let results = this.parsePlayer(args, callerInfo);
        if (results != false) {
            let embed = new MessageEmbed()
                .setTitle("Cheat check results for " + results.playerName + " (" + results.playerId + "):")
                .setColor(0xD2691E)
                .setDescription("Testing embeds again");

            await message.channel.send(embed);
        }
        //return await parsePlayer(args, callerInfo);
    },  
    cheatCheckGuild: async function(args, callerInfo) {
        return await cheatCheckGuild(args, callerInfo);
    },
    parsePlayer: async function(args, callerInfo) {
        if (!args || !args.length) {
            this.sendError("Rwww. ('No.').", "You didn't give me an ally code!");
            return false;
        }
    },
    sendError: function(title, errorMessage) {
        errorEmbed = new MessageEmbed().setTitle(title).setColor(0xaa0000).setDescription(errorMessage);
        this.message.channel.send(errorEmbed);
    }
}

function getErrorEmbed(title, errorMessage)
{
    var embed = {};

    embed.title = title;
    embed.color = 0xaa0000;
    embed.description = errorMessage;
    return embed;
}

function likelihoodSymbol(likelihood)
{
    if (likelihood >= criticalThreshold) {
        return ":rotating_light:";
    } else if (likelihood >= warningThreshold) {
        return ":thinking:";
    } else
    {
        return ":white_check_mark:";
    }
}

async function parsePlayer(args, callerInfo) {
    
    var embedToSend = createEmbed();

    if (!args || !args.length) {
		return getErrorEmbed("Rwww. ('No.').", "You didn't give me an ally code!");
    }
    var playerId = args[0].replace(/-/g, "");
    var detailed = false;

    if (args.length > 1)
    {
        for (var argIndex = 1; argIndex < args.length; argIndex++)
        {
            if (args[argIndex] == "d" || args[argIndex] == "details")
            {
                detailed = true;
            }
        }
    }

    if (playerId.length != 9)
    {
        return getErrorEmbed("Rwww. ('No.').", "You gave me an invalid ally code (" + playerId + ")!");
    }

    let payload = {
        allycode:playerId, 
        language: "eng_us",
        project: {
            name: 1,
            roster: {
                nameKey: 1,
                rarity: 1,
                gear: 1,
                skills: 1
            }
        }
    };

    database.db.one("INSERT INTO cheating_queries(allycode, timestamp) VALUES($1, $2) RETURNING *",
                    [playerId, new Date()]);
    
    var unitsList = await playerUtils.getUnitsList();

    let fetchPlayer = await swapi.fetchPlayer(payload).catch(error => {
        return(getErrorEmbed("Huac ooac. ('Uh oh.').", error));
    });

    var player = fetchPlayer.result;
    if (!player)
    {
        return getErrorEmbed("Aorcro rarrraahwh. ('Try again.')", "A player was not found with ally code (" + playerId + ").");
    }
    var playerName = player[0].name;

    var embedToSend = createEmbed();

    embedToSend.title = "Cheat check results for " + playerName + " (" + playerId + "):";
    
    for (var checkCharDisplayName in cheating_data)
    {
        var checkChar = cheating_data[checkCharDisplayName].character_name;
        var charResultsString = "";
        var fv = { name: null, value: null, inline: false };

        var rareCharQuery = jp.query(player, "$..roster[?(@.nameKey=='" + checkChar + "')]");
        

        fv.name = "__**" + checkCharDisplayName + "**__";

        if (rareCharQuery.length == 0)
        {
            charResultsString = "Not activated.";
            fv.name = ":zero: " + fv.name;
        } 
        else if (rareCharQuery[0].rarity < cheating_data[checkCharDisplayName].min_stars_to_check)
        {
            charResultsString = "Only checking units at " + cheating_data[checkCharDisplayName].min_stars_to_check + "★";
            fv.name = ":no_mouth: " + fv.name + " (" + rareCharQuery[0].rarity + "★)";
        }
        else
        {

            let results = await checkRequirements(player[0], cheating_data[checkCharDisplayName], unitsList);

            fv.name = (likelihoodSymbol(results.likelihood) + " " + fv.name + " (" + rareCharQuery[0].rarity + "★)").trim();

            var likelihoodString = results.likelihood + "% chance that " + playerName + " cheated.";
            
            charResultsString += likelihoodString;
            if (detailed || results.likelihood >= warningThreshold)
            {
                charResultsString += "```";
                for (var charIndex = 0; charIndex < results.characters.length; charIndex++)
                {
                    var character = results.characters[charIndex];
                    charResultsString += character.characterName + ", " + 
                                        character.stars + "★, " + 
                                        "G" + character.gear + ", " + 
                                        character.zetas + " zetas\r\n";
                }
                charResultsString += "```";
            }

        }

        fv.value = charResultsString;
        
        embedToSend.fields.push(fv);
    }

    return embedToSend;
}

async function checkRequirements(player, characterObj, unitsList)
{
    var numCharacters = characterObj.requirements.num_characters;
    var minStars = characterObj.requirements.min_stars;

    var faction = characterObj.requirements.faction;
    var faction_units = [];

    if (faction)
    {
        faction_units = unitsList.filter((u) => 
            u.categoryIdList.includes(faction) &&
            !characterObj.requirements.characters.find((c) => c.name == u.nameKey)
            );
    }

    // console.log(faction);
    // console.log(faction_units);

    var resultsSet = [];
    
    for (var charIndex in characterObj.requirements.characters)
    {
        var characterRequirement = characterObj.requirements.characters[charIndex];

        var resultObj = checkCharacter(player, characterRequirement);

        if (resultObj != null && resultObj.character.stars >= 7) 
            resultsSet.push(resultObj);
    }

    for (var fuix = 0; fuix < faction_units.length; fuix++)
    {
        var characterRequirement = characterObj.requirements.faction_characters_default;

        var resultObj = checkCharacter(player, characterRequirement, faction_units[fuix].nameKey);

        if (resultObj != null && resultObj.character.stars >= 7) 
            resultsSet.push(resultObj);
    }

    // sort in numeric order
    resultsSet = resultsSet.sort((a, b) => b.impact - a.impact);

    // console.log(resultsSet);
    
    var results = {
        characters: [],
        likelihood: 100
    };
    
    // only look at the first numCharacters impacts on likelihood, since those are the characters that are most
    // likely to have been used in the event
    for (var ix = 0; ix < numCharacters && ix < resultsSet.length; ix++)
    {
        results.likelihood -= resultsSet[ix].impact;
        results.characters.push(resultsSet[ix].character);
    }

    // can't be less than a 0% chance or above 100%
    results.likelihood = Math.min(Math.max(0, results.likelihood), 100);

    //console.log(results);

    return results;
}

function checkCharacter(player, characterRequirement, characterName)
{
    // console.log("req = " + characterRequirement);
    if (!characterName)
        characterName = characterRequirement.name;

    var resultObj = {
        impact: 0,
        character: {
            characterName: characterName,
            stars: 0,
            gear: 0,
            zetas: 0
        }
    };

    var charQueryResults = [];

    try {
        // console.log("looking for: " + characterName);
        charQueryResults = jp.query(player, "$..roster[?(@.nameKey=='" + characterName + "')]");
    } catch(error) {
        console.error(error);
    }

    if (charQueryResults.length > 0) 
    {
        var actualChar = charQueryResults[0];

        if (actualChar.rarity < 7)
            return null;

        resultObj.character.stars = actualChar.rarity;
        resultObj.character.gear = actualChar.gear;
        
        // find out how many zetas the player put on the character
        var zetaQueryResults = jp.query(actualChar, "$..skills[?(@.isZeta==true && @.tier == 8)]");
        resultObj.character.zetas = zetaQueryResults.length;
        
        resultObj.impact = characterRequirement.gear_scale[resultObj.character.gear] 
            /* + characterRequirement.stars_scale[resultObj.character.stars] */ // since we're looking at 7* only, ignore stars
            + characterRequirement.zeta_scale[resultObj.character.zetas];

    }

    return resultObj;
}


async function cheatCheckGuild(args, callerInfo)
{
    var embedToSend = createEmbed();

    if (!args || !args.length) {
		return getErrorEmbed("Rwww. ('No.').", "You didn't give me an ally code!");
    }

    var allycode = playerUtils.cleanAndVerifyStringAsAllyCode(args[0]);

    var guild = await guildUtils.getGuildData(allycode);


    embedToSend.title = "Cheat check results for guild **" + guild.name + "**:";

    var guildAllyCodes = new Array();

    for (var m = 0; m < guild.roster.length; m++)
    {
        var memberAllyCode = guild.roster[m].allyCode;
     
        guildAllyCodes.push(memberAllyCode);
    }


    discordUtils.updateDiscordMessage(callerInfo.bot, callerInfo.channelId, callerInfo.messageId, "Found guild **" + guild.name + "**.  Checking " + guildAllyCodes.length + " players, please wait...");

    var unitsList = await playerUtils.getUnitsList();
    var foundCheater = false;
    var embedDescription = "Here are the players that have a high likelihood of cheating:\r\n";
    embedDescription += "```";
    
    
    let payload = {
        allycodes: guildAllyCodes, 
        language: "eng_us",
        project: {
            name: 1,
            allyCode: 1,
            roster: {
                nameKey: 1,
                rarity: 1,
                gear: 1,
                skills: 1
            }
        }
    };

    let { result, error, warning } = await swapi.fetchPlayer(payload);

    for (var playerIndex = 0; playerIndex < result.length; playerIndex++)
    {
        var player = result[playerIndex];

        for (var checkCharDisplayName in cheating_data)
        {
            var checkChar = cheating_data[checkCharDisplayName].character_name;
            
            // confirm they have the character activated
            var rareCharQuery = jp.query(player, "$..roster[?(@.nameKey=='" + checkChar + "')]");
            if (rareCharQuery.length == 0 || rareCharQuery[0].rarity != 7) continue;

            let results = await checkRequirements(player, cheating_data[checkCharDisplayName], unitsList);

            // something suspicious with this player.  Add to the list and move to the next player
            // otherwise, check the next character
            if (results.likelihood >= criticalThreshold)
            {
                foundCheater = true;
                embedDescription += player.allyCode + " (" + player.name + ")\r\n"

                break;
            }
        }
    }

    if (foundCheater)
        embedDescription += "```";
    else {
        embedDescription = "No players in **" + guild.name + "** are likely to have cheated at checked events.";
    }

    embedToSend.description = embedDescription;

    return embedToSend;
}