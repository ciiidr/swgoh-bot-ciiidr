const ApiSwgohHelp = require('api-swgoh-help');

const swapi = new ApiSwgohHelp({
    "username":process.env.API_USERNAME,
    "password":process.env.API_PASSWORD,
});

let sessionMods = [];

module.exports.parsePlayer = async function(playerId) {
    return await parsePlayer(playerId)
}

async function parsePlayer(playerId) {

    let payload = {allycode:playerId};
    let { result, error, warning } = await swapi.fetchPlayer( payload );

    const player = result;

    var characters = player[0].roster;

    for (var i=0; i<characters.length; i++) {
        var character = characters[i];
        //var character = characters[10];
        var mods = character.mods;
        var name = character.defId;
        if (mods.length > 0) {
            parseMods(mods, name);
        }

    }

    const sortedMods = sessionMods.sort((a,b) => parseFloat(b.quality) - parseFloat(a.quality)).filter((c,d) => d >=sessionMods.length - 10);
    return formatModData(sortedMods);
}

function parseMods(mods, name) {

    for (var i=0; i<mods.length; i++) {
        var mod = mods[i];
        var eMod = Object.create(Mod).build(mod);

        var types = {
            0: "Square",
            1: "Arrow",
            2: "Diamond",
            3: "Triangle",
            4: "Circle",
            5: "Cross"
        }

        var modId = eMod.mod.id;
        var type=types[i];

        var stats = eMod.secondaryStats;

        var action = "3", primaryStat = "none", overallQuality = 0;
        for (var j=0; j<stats.length; j++) {
            var currentMod = stats[j];
            if (currentMod.ranking == 'primary') {
                action = currentMod.action;
                primaryStat = currentMod.type;
            }
            overallQuality += parseInt(currentMod.quality);
        }

        overallQuality = (overallQuality/400)*100;





        sessionMods.push({
            "id": modId,
            "name": name,
            "type": type,
            "set": eMod.set,
            "tier": eMod.tier,
            "primaryStat": eMod.primaryStat,
            "action": action,
            "quality": overallQuality
        })

    }
}

var Mod = Object.create(null, {
    set: {
        get: function() {
            var sets = {
                1: "Health",
                2: "Offense",
                3: "Defense",
                4: "Speed",
                5: "Critical Chance",
                6: "Critical Damage",
                7: "Potency",
                8: "Tenacity"
            }
            var modset = sets[this.mod.set];
            return modset;
        }
    },
    tier: {
        get: function() {
            var tiers = {
                1: "Grey",
                2: "Green",
                3: "Blue",
                4: "Purple",
                5: "Gold"
            }
            var modtier = tiers[this.mod.tier];
            return modtier;
        }
    },
    primaryStat: {
        get: function() {
            var statLabels = {
                1: "Flat Health",
                5: "Flat Speed",
                16: "% Crit Damage",
                17: "% Potency",
                18: "% Tenacity",
                28: "Flat Protection",
                41: "Flat Offense",
                42: "Flat Defense",
                48: "% Offense",
                49: "% Defense",
                52: "% Accuracy",
                53: "% Crit Chance",
                54: "% Crit Avoidance",
                55: "% Health",
                56: "% Protection"
            }
            return statLabels[this.mod.primaryStat.unitStat];
        }
    },
    secondaryStats: {
        get: function() {
            var secondaryStats = getSecondaryStats(this.mod.primaryStat, this.mod.secondaryStat);

            return secondaryStats;
        }
    },
    build: {
        value: function(mod) {
            this.mod = mod;
            return this;
        }
    },
});

function getSecondaryStats(primaryStat, stats) {

    console.log(stats);
    throw new Error ("End Here");

    var statQuality = [];

    for (var i=0; i<stats.length; i++) {
        statQuality[i] = checkStatQuality(primaryStat, stats[i]);
    }

    return statQuality;

}

function checkStatQuality(primaryStat, stat) {

    var statLabels = {
        1: "Flat Health",
        5: "Flat Speed",
        17: "% Potency",
        16: "% Crit Damage",
        18: "% Tenacity",
        28: "Flat Protection",
        41: "Flat Offense",
        42: "Flat Defense",
        48: "% Offense",
        49: "% Defense",
        52: "% Accuracy",
        53: "% Crit Chance",
        54: "% Crit Avoidance",
        55: "% Health",
        56: "% Protection"
    }

    var statMaxPerRoll = {
        1: 428,
        5: 6,
        17: 2.25,
        18: 2.25,
        28: 830,
        41: 46,
        42: 9,
        48: 0.56,
        49: 1.70,
        53: 2.25,
        55: 1.13,
        56: 2.33
    }

    var statMinPerRoll = {
        1: 214,
        5: 3,
        17: 1.12,
        18: 1.12,
        28: 415,
        41: 23,
        42: 4,
        48: 0.28,
        49: 0.85,
        53: 1.12,
        55: 0.56,
        56: 1.12
    }

    var statId = stat.unitStat;
    var roll = stat.roll;
    var value = stat.value;
    var label = statLabels[statId];

    var quality = value/(statMaxPerRoll[statId]*roll);

    quality = Math.round(quality*100);


    //This is close, but...what we want to do is give a weight if the secondary stat has compatibility with the primary stat
    var action = getStartingAction(roll, quality);
    var primaryModifier = getPrimaryActionModifier(primaryStat, statId);
    action += primaryModifier;

    var ranking = roll>4 ? 'primary' : 'secondary';
    // Checking set/stat compatibility


    return {
        type: label,
        quality: quality,
        action: action,
        ranking: ranking
    };

}

function getStartingAction(roll, quality) {
    if (roll>2) {
        if (quality > 85) {
            return 4;
        }
        if (quality < 70) {
            return 2;
        }
    }
    return 3;
}

function getPrimaryActionModifier(primaryStat, statId) {
    var compatibleStatistics = {
        '5': [1,5,17,18,28,41,42,48,49,53,55,56],
        '16': [5,41,48,53],
        '17': [5, 17],
        '18': [1,5,17,28,42,49,55,56],
        '48': [5,41,48,53],
        '49': [1,5,18,28,42,49,55,56],
        '52': [5,16,41,48,52,53],
        '53': [5,41,48,53],
        '54': [1,5,28,42,49,55],
        '55': [1,5,18,28,42,49,55,56],
        '56': [1,5,18,28,42,49,55,56]
    }

    var primaryStatId = primaryStat.unitStat;
    var primaryStatIdText = primaryStatId.toString().split(".")[0];

    var compatibleStatisticsArray = compatibleStatistics[primaryStatIdText];

    if (compatibleStatisticsArray.includes(statId)) {
        return 1;
    }
    return 0;

}

function formatModData(mods) {

    var embed = {
        "title": "Worst 10 mods",
        "description": null,
        "color": 0xD2691E,
        "author": {
            "name": "WookieeBot"
        },
        "fields": []
    };

    for (let i=0; i<mods.length; i++) {
        const currentMod = mods[i];
        embed.fields.push(currentMod.name+" : "+currentMod.type);
    }

    return embed;
}

function getModQuality(set, primaryStat, secondaryStats) {
    var set_primary_match_ranking = get_set_primary_compatibility();
    var primary_secondary_match_ranking = get_primary_secondary_compatibility();
    var speed = get_speed_ranking();
    var secondary_quality = get_secondary_quality();

    var quality_weights = {
        set_primary_ranking : 4,
        primary_secondary_ranking: 3,
        speed_ranking: 3,
        secondary_quality: 1
    }

    var overall_quality =
          (set_primary_match_ranking*quality_weights.set_primary_ranking)
        + (primary_secondary_match_ranking*quality_weights.primary_secondary_ranking)
        + (speed_ranking*quality_weights.speed_ranking)
        + (secondary_quality*quality_weights.secondary_quality);

    return overall_quality;
}