
var logger = require('winston');
require('dotenv').config({path:__dirname+'/./../../.env'})
var database = require("../database");
var guildRefresh = require("../commands/guild/refresh");
// var goals = require("../modules/goals");

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';

database.db.any('SELECT * FROM guild').then( async (registeredGuilds) => {
    for (var g in registeredGuilds)
    {
        var guild = registeredGuilds[g];
        logger.log("debug", "refreshing data for " + guild.guild_name);
        
        try {
            await guildRefresh.refreshGuildData([guild.allycode]);
            logger.log("debug", "Refreshed guild data for " + guild.guild_name);
        } catch (e)
        {
            logger.log("error", "Error refreshing guild data for " + guild.guild_name + ". Details: " + e);
        }        
   }
});

